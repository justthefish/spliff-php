<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <title>Clip4me <?php echo $title?></title>
    <meta name="keywords" content="<?php echo $keywords?>">
    <meta name="description" content="<?php echo $description?>">
    <?php foreach ($styles as $path => $data):?>
    <link type="text/css" href="<?php echo BASE_URL.'/'.$path?>" rel="stylesheet" media="<?php echo $data['media']?>" />
    <?php endforeach;?>

    <?php foreach ($scripts as $path):?>
    <script type="text/javascript" charset="utf-8" src="<?php echo BASE_URL.'/'.$path?>"></script>
    <?php endforeach;?>
</head>
<body>
<div style="text-align:center;">
<div style="display:block;">
<pre>
       _       _    __ _             _              _   
 _ __ (_)_  __| |_ / _| |_ _ __   __| |  _ __   ___| |_ 
| '_ \| \ \/ /| __| |_| __| '_ \ / _` | | '_ \ / _ \ __|
| |_) | |>  < | |_|  _| |_| |_) | (_| |_| | | |  __/ |_ 
| .__/|_/_/\_(_)__|_|  \__| .__/ \__,_(_)_| |_|\___|\__|
|_|                       |_|                           
</pre>
</div>
</div>

<div class="content">
    <ul class="menu">
        <?php if ($user instanceof User) {?>
        <li class="pull-right"><a href="<?php echo BASE_URL.'/account/logout'?>">logout</a></li>
        <li class="pull-right"><?php echo $user->login;?></li>
        <?php } ?>
        <li><a href="<?php echo BASE_URL?>/">home</a></li>
        <li><a href="<?php echo BASE_URL?>/faq">faq</a></li>
        <li><a href="<?php echo BASE_URL?>/account/install">installation</a></li>
    </ul>
    <div class="clearfix"></div>
</div>
<center>
<?php echo $content?>
</center>
<?php echo $footer?>

</body>
</html>

<div class="content">
    <h1>How to install</h1>

    <p>First of all, there is your secret key:</p>

    <p>
        <input type="text" 
                style="padding:5px; font-size:120%; width:60%; display:block;" 
                readonly="true" 
                value="<?php echo Security::gen_key($user->login);?>" />
    </p>

    <p>
        There is a slightly different installation process for different operating systems.
        Please read appropriate section.
    </p>
    <ul>
        <li><a href="#linux">Linux</a></li>
        <li><a href="#windows">Windows</a></li>
        <li><a href="#macos">Mac OS</a></li>
    </ul>


    <h2 id="linux">Linux</h2>

    <p>Download package for linux and run install.sh</p>
    <p>You will be asked for your  secret key (see in the beginning of page).</p>
    <p>Then you have to manually bind program to global hotkey, accroding to instructions for your desktop environment.</p>
    <p>
        For example, in XFCE you should open Settings -> Settings Manager -> Keyboard and open "Application Shortcuts" tab.
        There you press "Add" button and enter following string in "command" field of dialog window:
    </p>
    <code>
    ~/.clip4me/clip4me.sh
    </code>
    <p>and then bind it to your favorite key (mine is on Super+z). After restart of your X session, Clip4me will be available on this shortcut.</p>
    <h2 id="windows">Windows</h2>
    <p>In development, not complete yet.</p>
    <h2 id="macos">Mac OS</h2>
    <p>In development, not complete yet.</p>
</div>

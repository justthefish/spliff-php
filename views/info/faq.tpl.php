<div class="content">
<h3>What's this?</h3>
<p>It is a small screenshot storing service, written out of pure curiosity,</p>
<p>Now it appears to be somewhat functional, so feel free to use it.</p>
<p>Warning: service still in early alpha stage, do not rely on service for storing critical information.</p>

<h3>How do i install client?</h3>
<p>Look at our <a href="<?php echo BASE_URL?>/account/install">installation instructions</a>. You have to be registered and logged in to read them.</p>
<p>Currently only Linux client is released, clients for other OSes are coming soon&trade;.</p>

<h3>How do i make screenshots?</h3>
<p>Press designated shortcut, drag rectangle around area you wish to publish and release mouse button.</p>
<p>Then choose between saving screenshot locally or publishing to our server.</p>
<p>You can just double-click any window header to publish whole contents of this window.</p>

<h3>Are there any restrictions?</h3>
<p>Yes, the maximum lifetime of picture is limited to 14 days.</p>
<p>There is no limit to number oof pictures (yet).</p>

<h3>Some restrictions are unacceptable to me, how can i lift them?</h3>
<p>Sadly, this service is only an experimental playground now, so there is no way to get rid of restrictions.</p>
<p>Maybe in future this will change. Maybe.</p>

</div>

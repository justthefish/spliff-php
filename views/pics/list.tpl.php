<div class="content">
    <div class="pics">
        <?php
        foreach ($pics as $pic) { ?>
            <a href="<?php echo $pic["img"]?>" target="_blank">
                <img src="<?php echo $pic["thumb"]?>" width="200" height="200"/>
            </a>
        <?php } ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * 
 **/
class About_Controller extends Controller {
    
    public function action_index() {
    
        $this->template->content = View::factory('page/about');
    }
}

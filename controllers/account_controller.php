<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * account contoller
 **/
class Account_Controller extends Controller {
    
    public function action_index() {
        if (!$this->auth->loggedIn()) {
            $this->request->redirect('/account/login');
        } 
        $pics = array();
    
        $user = $this->auth->getUser();
        $userdir = USERDIR_PATH.'/'.$user->login;

        $map_func = function ($name) use($user) {
            if (strpos($name, '.jpg')) {
                return array(
                    "img" => BASE_URL.'/images/'.$user->login.'/'.$name,
                    "thumb" => BASE_URL.'/images/'.$user->login.'/t/'.$name
                );
            }
        };
        $pics = $this->scanByModifiedTime($userdir);
        $pics = array_filter(array_map(
            $map_func,
            $pics
        ));
    
        $this->template->content = View::factory('pics/list')
            ->bind('pics', $pics)
            ->set('user', $this->auth->getUser());
    }

    public function action_login() {
        $view = View::factory('account/login');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if($this->auth->login($_POST["email"], $_POST["password"], true)) {
                $this->request->redirect('/'); //redirect to root
            } else {
                $view->bind('email', $_POST["email"])->set('errors', array('Wrong username or password'));
            }
        }

        $this->template->content = $view;
    }

    public function action_logout() {
        $this->auth->logout();
        $this->request->redirect('/');
    }

    public function action_register() {
        $user = new User();
        $errors = $arr = array();
        if ($_SERVER["REQUEST_METHOD"] == 'POST') {

            //print_r($arr);
            $arr['login'] = trim($_POST['login']);
            $arr['password'] = (empty($_POST['password']) ? null : $this->auth->hash(trim($_POST['password'])));
            $arr['email'] = trim($_POST['email']);

            $arr = array_filter($arr);
            $user->import($arr);
            $errors = $user->errors;

            if (!isset($errors['email']) && $user->getByEmail($arr['email'])) {
                $errors[] = 'Email taken, sorry.';
            }
            if (!isset($errors['login']) && is_dir(USERDIR_PATH.'/'.$arr['login'])) {
                $errors[] = 'Login taken. sorry.';
            }

            if (empty ($errors)) {
                $user->save();

                mkdir(USERDIR_PATH.'/'.$user->login);
                mkdir(USERDIR_PATH.'/'.$user->login.'/t');
                $this->auth->forceLogin($user->email);

                $this->request->redirect('/');
            }
        }

        $this->template->content = View::factory('account/register')
            ->set('user', $user)
            ->set('errors', $errors);
    }

    function action_test() {
        $user = new User();
        print_r($user->import(array(
            'id' => '2',
            'login' => 'test3',
            'password' => 'trololo',
            'email' => 'nono@email.com'
            ))->save());

    }


    function action_install() {
        if (!$this->auth->loggedIn()) {
            $this->request->redirect('/account/login');
        } 
        $this->template->content = View::factory('info/install')
            ->set('user', $this->auth->getUser());
    }


    private function scanByModifiedTime($dir) {
        $ignored = array('.', '..', '.svn', '.htaccess', '.git');

        $files = array();    
        foreach (scandir($dir) as $file) {
            if (in_array($file, $ignored)) continue;
            $files[$file] = filemtime($dir . '/' . $file);
        }
        
        arsort($files);
        $files = array_keys($files);

        return ($files) ? $files : false;
    }

}

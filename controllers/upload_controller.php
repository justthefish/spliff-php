<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * upload controller
 **/

class Upload_Controller extends Controller {

    public function before() {
        parent::before();
        $this->autorender = false;
    }

    public function action_index() {

        //$secretkey = md5(substr($_POST['login'], 0, 17).substr(md5($_POST['login']),-14, 21).'swirly curly br4ce5');
        $secretkey = Security::gen_key($_POST['login']);
        //$root = '/home/thefish/projects/ferrum.com.ru/www';

        if (isset($_POST['key']) && $_POST['key']==$secretkey && !empty($_FILES['file']['tmp_name'])) {
            //$new_filename = '/clip4me/images/';
			$new_filename = '';
            $new_filename .= '/'.$_POST['login'].'/';
            $new_filename .= Security::gen_uuid(32).".jpg";
            //if (move_uploaded_file($_FILES['file']['tmp_name'],$root.$new_filename)) {
            if (move_uploaded_file($_FILES['file']['tmp_name'], USERDIR_PATH.$new_filename)) {
                $image = imagecreatefromjpeg(USERDIR_PATH.$new_filename);
                $filename = USERDIR_PATH.
                    '/'.$_POST['login'].'/t/'.
                    pathinfo($new_filename, PATHINFO_BASENAME);

                $thumb_width = 200;
                $thumb_height = 200;

                $width = imagesx($image);
                $height = imagesy($image);

                $original_aspect = $width / $height;
                $thumb_aspect = $thumb_width / $thumb_height;

                if ( $original_aspect >= $thumb_aspect )
                {
                       // If image is wider than thumbnail (in aspect ratio sense)
                       $new_height = $thumb_height;
                          $new_width = $width / ($height / $thumb_height);
                }
                else
                {
                       // If the thumbnail is wider than the image
                       $new_width = $thumb_width;
                          $new_height = $height / ($width / $thumb_width);
                }

                $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
                // Resize and crop
                imagecopyresampled($thumb,
                    $image,
                    0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                    0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                    0, 0,
                    $new_width, $new_height,
                    $width, $height);
                imagejpeg($thumb, $filename, 80);

                //echo 'http://'.$_SERVER['HTTP_HOST'].$new_filename;
                $this->request->response = BASE_URL.'/images'.$new_filename;
            } else {
                $this->request->response = 'upload failed to:'.$new_filename;
            }

        } else {
            $this->request->response = "empty file or wrong key";
        } 
    }

}


<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * Faq controller
 **/
class Faq_Controller extends Controller {
    
    public function action_index () {
        $this->template->content = View::factory('info/faq');
    }
}
?>

<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * 
 **/
class Contact_Controller extends Controller {
    
    public function action_index() {
    
        $this->template->content = View::factory('page/contact');
    }
}

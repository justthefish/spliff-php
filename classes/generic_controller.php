<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * controller class
 **/
class Generic_Controller {
    
    public $request;
    public $template = 'layout/main';
    public $auth = NULL;
    public $autorender = TRUE;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function before() {
        $this->auth = new Auth();
        if($this->autorender == TRUE) {
            $this->template = View::factory($this->template);
            $this->template->title = '';
            $this->template->keywords = '';
            $this->template->description = '';
            $this->template->styles = array();
            $this->template->scripts = array();
            $this->template->footer = '';
            $this->template->content = '';
            $this->template->set('user', $this->auth->getUser());
        }
        
    }
    public function after() {

        if($this->autorender == TRUE) {
            $styles = array(
                'css/style.css' => array('media' => 'screen, projection'),
                'css/bootstrap.css' => array('media' => 'screen, projection'),
                'css/bootstrap-responsive.css' => array('media' => 'screen, projection'),
            );
            $scripts = array(
                'js/jquery.min.js',
                'js/bootstrap.js',
            );

            $this->template->scripts = array_merge($scripts, $this->template->scripts);
            $this->template->styles = array_merge($this->template->styles, $styles);
            
            $this->request->response = $this->template;
        }
    }
}

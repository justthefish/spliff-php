<?php
class DB {
    
    public static $instance; //instance of 
    public $db;
    protected $query;

    public function __construct() {
        $this->db = new PDO('sqlite:'.APP_PATH.'/db/db.sqlite');

        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

    }

    //singleton
    public static function me() {
        if (!isset(DB::$instance)) {
            DB::$instance = new DB();
        }

        return DB::$instance;
    }

    //this method if for selects
    //returns dataset
    public function query() {
        $results = $this->db->query($this->query);
        $i = 0;
        $response = array();
        foreach ($results as $result) {
            $response[] = $result;
        }
        if (count($response) > 1) {
            $result = $response;
        }elseif (count($response) == 0) {
            return null;
        } else {
            $result = $response[0];
        }
        $this->query = NULL;
        return $result;
    }

    //this is for update/delete/drop etc, 
    //doesnt return dataset
    public function exec($query = NULL) {
        if (is_null($query)) {
            $query = $this->query;
        }
        $result = $this->db->exec($query);
        $this->query = NULL; //сбрасываем текущий запрос
        return $result;
    }

    public function shutdown() {
        return $this->db = NULL;
    }

    public function select(Model $model) {

        $this->query = 'select ';

        $this->query .= implode(',', array_keys($model->fields));

        $this->query .= ' from '.$model->table;

        return $this;

    }

    public function where($conditions) {
        $this->query .= ' where '.$conditions;
        return $this;
    }

}
?>

<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * session class
 **/
class Session {
    
    protected $_lifetime = 86400;
    protected $_name = 'ipix_session';
    protected $_data = array();
    protected $_destroyed = NULL;
    public static $instance = NULL;

    function __construct($id = NULL)
    {
        $this->read($id);
    }

    public function __toString() {
        return base64_encode(serialize($this->_data));
    }

    public static function me($id = NULL) {
        if (!isset(Session::$instance)) {
            Session::$instance = $session = new Session($id);
            register_shutdown_function(array($session, 'write'));
        }
        return Session::$instance;
    }

    public function get($key, $default = NULL){
        return array_key_exists($key, $this->_data) ? $this->_data[$key] : $default;
    }

    public function get_once($key, $default = NULL)
    {
        $value = $this->get($key, $default);

        unset($this->_data[$key]);

        return $value;
    }

    public function set($key, $value)
    {
        $this->_data[$key] = $value;

        return $this;
    }

    public function bind($key, & $value)
    {
        $this->_data[$key] =& $value;

        return $this;
    }

    public function delete($key)
    {
        $args = func_get_args();

        foreach ($args as $key)
        {
            unset($this->_data[$key]);
        }

        return $this;
    }



    public function read($id) {
        try {
            if (is_string($data = $this->_read($id))) {
                $data = base64_decode($data);
                $data = unserialize($data);
            }    
        } catch (Exception $e) {
            echo Debug::getExceptionTraceAsString($e);
            //throw $e;
        }
        if (is_array($data)) {
            $this->_data = $data;
        }
    }


    public function regenerate() {
        return $this->_regenerate();
    }


    public function write() {
        if (headers_sent()) {
            return false;
        }

        $this->_data['lastActive'] = time();
        try {
            return $this->_write();
        } catch (Exception $e) {
            echo Debug::getExceptionTraceAsString($e);
            //throw $e;
        }
    }


    public function destroy() {
        if ($this->_destroyed === FALSE) {
            if ($this->_destroyed = $this->_destroy()) {
                $this->_data = array();
            }
        }
        return $this->_destroyed;

    }

    protected function _destroy() {
        session_destroy();
        $status = !session_id();
        if ($status) {
            //set cookie to null and expired
            setcookie($this->_name, NULL, -86400);
        }
        return $status;
    }



    //@todo перенести на уровень выше - детали реализации
    public function id() {
        return session_id();
    }

    //@todo перенести на уровень выше - детали реализации
    protected function _read($id = NULL) {

        //@todo hardcoded
        //session_set_cookie_params(lifetime, path, domain, secure, httponly);
        session_set_cookie_params($this->_lifetime);
        //@todo double-check disabling session cache;
        session_cache_limiter(FALSE);

        if ($id) {
            session_id($id);
        }
        session_start();
        $this->_data =& $_SESSION; //по ссылке!!!
        return NULL;
    }

    //@todo перенести на уровень выше - детали реализации
    public function _regenerate() {
        return session_regenerate_id();
    }

    //@todo перенести на уровень выше - детали реализации
    protected function _write() {
        session_write_close();
    }

}
   

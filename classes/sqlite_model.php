<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * sqlite model
 **/
class Sqlite_Model extends Model {
    

    public function load($id) {
        $this->data = DB::me()
            ->select($this)
            ->where('id = '.$id)
            ->query();
        
        foreach ($this->fields as $field => $meta) {
            $this->{$field} = $this->data[$field];
        }
        return $this;
    }


    public function update() {
        $model = $this;

        $query = 'update '.$this->table.' set ';
        $fields = array();
        $identifier = array();
        foreach ($this->fields as $field => $meta) {
            if ($meta['type'] != 'identifier') {
                if (isset($model->{$field})) {
                    $fields[] = '"'.$field.'" = \''.$model->{$field}.'\'';
                }
            } else {
                $identifier['field'] = $field;
                $identifier['value'] = $model->{$field};
            }
        }
        if (empty($fields)) {
            throw new DBException('Tring to update empty model. Lol?');
        }
        $query.= ' '.implode(',', $fields).' ';
        $query .= ' where '.$identifier['field']
            .' = '.$identifier['value'];

        DB::me()->exec($query);
        return $model;

    }

    public function insert() {
        $model = $this;

        $query = 'insert into '.$this->table;
        $insert_fields = array();
        $values = array();
        foreach ($this->fields as $field => $meta) {
            if ($meta['type'] != 'identifier') {
                $insert_fields[] = '"'.$field.'"';
                $values[] = '\''.(isset($this->data[$field]) ? $this->data[$field] : ( isset($this->{$field}) ? $this->{$field} : NULL)).'\'' ;
            }
        }
        $query.= ' ('.
            implode(',', $insert_fields)
            .') values ('.
            implode(',', $values)
            .')';
        DB::me()->exec($query);
        $result = DB::me()->db->lastInsertId();
        return $result;
    }

    public function save() {
        $model = $this;
        foreach ($this->fields as $field => $meta) {
            if ($meta['type'] == 'identifier' && isset($model->{$field})) {
                return $this->update();
            }
        }
        return $this->load($this->insert());
    }

    public function delete(Model $model) {
        foreach ($this->fields as $field => $meta) {
            if ($meta['type'] == 'identifier' && isset($model->{$field})) {
                return DB::me()->exec('delete from '.$model->table.' where '.$field.' = '.$model->{$field});
            }
        }
    }

    //setting up model from external array
    public function import($arr) {

        if (!is_array($arr)) {
            return null;
        }

        foreach ($this->fields as $field => $meta) {
            if (isset($arr[$field])) {
                if ( isset($this->fields[$field]['validate']) ) {
                    $this->data[$field] = $this->{$field} = $this->validate($arr[$field], $field);
                } else {
                    $this->data[$field] = $this->{$field} = $arr[$field];
                }
            } elseif(isset($this->fields[$field]['required'])) {
                $this->errors[$field] = "$field must be set!";
            }
             
        }
        return $this;
    }
    public function as_array() {
        return $this->data;
    }


    protected function validate($value, $field) {
        $rules = explode(" ", $this->fields[$field]['validate']);
        foreach($rules as $rule) {
            $result =  Validation::me()->{$rule}($field, $value);
            if ( $result !== TRUE ) {
                $this->errors[$field] = $result;
            }
        }
        return $value;
    }


}

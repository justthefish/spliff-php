<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * genericmodel class
 **/
abstract class Model {
    
    public $table;
    public $object_name;
    public $fields = array();
    public $data = array();
    public $errors = array();


    public function __construct() {
        $this->init();
        foreach($this->fields as $field => $meta) {
            $this->{$field} = NULL;
        }
    }

    public function table($table) {
        $this->table = $table;
        return $this;
    }

    public function name($name = NULL) {
        if (is_null($name)) {
            $this->object_name = strtolower($__CLASS__);
        } else {
            $this->object_name = $name;
        }
        return $this;
    }

    public function fields(array $fields) {
        $this->fields = $fields;
        return $this;
    }

    abstract function import($arr); //import data to model from array
    abstract function save(); //merge changes and update if id is provided else insert
    abstract function update(); //update, id required
    abstract function insert(); //insert new
    abstract function as_array(); //represent as array
}

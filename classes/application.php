<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * Application class
 **/
class Application {

    public static $instance;

    public function __construct() {
        // code...
    }

    public static function me() {
        if (!isset(Application::$instance)) {
            Application::$instance = new Application();
        }
        return Application::$instance;
    }

    public function run() {
        try {
            $request = Request::me();
            $request->execute();
        } catch (Exception $e) {
            throw $e;
        }
        echo $request->response;
    }
        
}
?>
